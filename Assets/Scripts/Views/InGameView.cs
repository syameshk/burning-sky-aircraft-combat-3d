﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameView : MonoBehaviour
{
    public GameObject InGameGroup;
    public GameObject GameInfoGroup;
    public Text scoreText;
    public GameObject PauseGroup;
    public GameObject LevelGroup;
    public Text resultText;
    public Text resultDeteils;

    public HealthBarUI healthBar;

    [Header("The Manager")]
    public GameSceneManager sceneManager;

    public void Init()
    {
        InGameGroup.SetActive(true);
        GameInfoGroup.SetActive(true);
        PauseGroup.SetActive(false);
        LevelGroup.SetActive(false);

        SetScore(0);
    }

    public void Reset()
    {
        InGameGroup.SetActive(false);
        GameInfoGroup.SetActive(false);
        PauseGroup.SetActive(false);
        LevelGroup.SetActive(false);
    }

    public void OnPauseButtonPressed()
    {
        PauseGroup.SetActive(true);
        GameInfoGroup.SetActive(false);

        sceneManager.OnPausePressed();
    }

    public void OnResumeButtonPressed()
    {
        PauseGroup.SetActive(false);
        GameInfoGroup.SetActive(true);

        sceneManager.OnResumePressed();

    }

    public void OnMenuButtonPressed()
    {
        Reset();
        //Move to menu
        sceneManager.OnMenuPressed();
    }

    public void OnPowerUpSelected(int index)
    {

    }

    public void SetScore(int score)
    {
        scoreText.text = string.Format("Score {0}", score);
    }

    public void OnLevelFailed(int level, int score)
    {
        GameInfoGroup.SetActive(false);
        LevelGroup.SetActive(true);

        resultText.text = "Level Failed!";
        resultDeteils.text = string.Format("Level {0} | Score {1}", level, score);
    }

    public void OnLevelCompleted(int level, int score)
    {
        GameInfoGroup.SetActive(false);
        LevelGroup.SetActive(true);

        resultText.text = "Level Completed!";
        resultDeteils.text = string.Format("Level {0} | Score {1}", level, score);
    }
}
