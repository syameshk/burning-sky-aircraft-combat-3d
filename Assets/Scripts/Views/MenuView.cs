﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuView : MonoBehaviour
{
    public GameObject MenuGroup;
    [Header("The Home Screen")]
    public GameObject HomeGroup;
    public Text scoreText;
    public Text levelText;

    [Header("The Level Selection Screen")]
    public GameObject LevelGroup;

    [Header("The Manager")]
    public GameSceneManager sceneManager;

    private int selectedLevel = 1;

    public void Init()
    {
        MenuGroup.SetActive(true);
        HomeGroup.SetActive(true);
        LevelGroup.SetActive(false);
    }

    public void Reset()
    {
        MenuGroup.SetActive(false);
        HomeGroup.SetActive(false);
        LevelGroup.SetActive(false);
    }

    public void SetHighScore(int score)
    {
        scoreText.text = string.Format("High Score : {0}", score);
    }

    public void SetLevel(int level)
    {
        levelText.text = string.Format("Level : {0}", level);
    }

    public void OnPlayButtonPressed()
    {
        sceneManager.OnPlayButtonSelected(selectedLevel);
    }

    public void OnLevelSelected(int index)
    {
        selectedLevel = index;
    }
}
