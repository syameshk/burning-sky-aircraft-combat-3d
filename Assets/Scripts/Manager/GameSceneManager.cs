﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSceneManager : MonoBehaviour
{

    public MenuView menuView;
    public InGameView gameView;

    private GameManger gameManger;

    private int level;

    // Start is called before the first frame update
    void Start()
    {
        gameManger = GetComponent<GameManger>();
        gameManger.OnLevelFailEvent.AddListener(OnLevelFaild);
        gameManger.OnLevelCompltedEvent.AddListener(OnLevelCompleted);
        gameManger.OnScoreUpdated.AddListener(OnScoreUpdated);

        //Set up the UI and plane
        Init();
    }

    void Init()
    {
        menuView.Init();
        gameView.Reset();

        

        //Load the top score
        int highScore = PlayerPrefs.GetInt("HS_KEY", 0);
        menuView.SetHighScore(highScore);

        //Load the level details
        int maxLevel = PlayerPrefs.GetInt("ML_KEY", 1);
        menuView.SetLevel(maxLevel);

        this.level = maxLevel;

        //Set up the game plane
        gameManger.Init();
    }

    public void OnPlayButtonSelected(int level)
    {
        menuView.Reset();
        gameView.Init();

        gameManger.Startlevel(this.level);
    }

    public void OnPausePressed()
    {
        Time.timeScale = 0;
    }

    public void OnResumePressed()
    {
        Time.timeScale = 1;
    }

    public void OnMenuPressed()
    {
        Time.timeScale = 1;
        gameManger.Reset();
        Init();
    }

    public void OnLevelStart(int level)
    {

    }

    private void OnScoreUpdated()
    {
        int score = GameManger.instance.score;
        gameView.SetScore(score);
    }

    public void OnLevelFaild()
    {
        int level = GameManger.instance.level;
        int score = GameManger.instance.score;
        //Show failed, menu
        gameView.OnLevelFailed(level-1, score);

        SaveDetails(level, score);
    }

    public void OnLevelCompleted()
    {
        //Set the high scpre, unlock the next level
        //Show sucess, menu

        int level = GameManger.instance.level;
        int score = GameManger.instance.score;
        //Show failed, menu
        gameView.OnLevelCompleted(level -1, score);

        SaveDetails(level, score);
    }

    private void SaveDetails(int level, int score)
    {
        //Load the top score
        int highScore = PlayerPrefs.GetInt("HS_KEY", 0);
        menuView.SetHighScore(highScore);

        //Load the level details
        int maxLevel = PlayerPrefs.GetInt("ML_KEY", 1);
        menuView.SetLevel(maxLevel);

        if (highScore < score)
            PlayerPrefs.SetInt("HS_KEY", score);

        if (maxLevel < level)
            PlayerPrefs.SetInt("ML_KEY", level);

        PlayerPrefs.Save();
    }
}
