﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManger : MonoBehaviour
{

    public static GameManger instance;
    public int score = 0;
    public int level = 1;

    private GameSetupHelper playerSetup;
    private LevelSetupHelper levelSetup;

    public UnityEvent OnLevelFailEvent;
    public UnityEvent OnLevelCompltedEvent;
    public UnityEvent OnScoreUpdated;

    private void Awake()
    {
        instance = this;

        playerSetup = GetComponent<GameSetupHelper>();
        levelSetup = GetComponent<LevelSetupHelper>();
    }

    public void Init()
    {
        playerSetup.Init();
    }

    public void Reset()
    {
        playerSetup.OnGameEnd();
        levelSetup.Reset();
    }

    public void Startlevel(int level = 0)
    {
        //Setup the level components
        this.level = level;
        this.score = 0;

        //start the game
        playerSetup.StartGame();
        playerSetup.OnPlayerReady.AddListener(OnPlayerReady);
    }

    public void Pause()
    {
        //Do something here?
    }

    public void LevelFail()
    {
        playerSetup.OnGameFailed();
        levelSetup.Reset();

        OnLevelFailEvent.Invoke();
    }

    public void LevelCompleted()
    {
        playerSetup.OnGameEnd();
        levelSetup.Reset();
        level++;
        AddScore(100);
        OnLevelCompltedEvent.Invoke();
    }

    private void OnPlayerReady()
    {
        //Unregister
        playerSetup.OnPlayerReady.RemoveListener(OnPlayerReady);

        //Start the enemy flow
        levelSetup.Init(level);

        //Start score calculation and stuff
    }

    private void OnPlayerLeave()
    {
        //Unregister
        playerSetup.OnPlayerLeave.RemoveListener(OnPlayerLeave);

    }

    public void AddScore(int amount)
    {
        score += amount;
        OnScoreUpdated.Invoke();
    }
}
