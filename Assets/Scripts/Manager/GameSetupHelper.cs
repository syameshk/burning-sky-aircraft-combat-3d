﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameSetupHelper : MonoBehaviour
{
    //The current selected plane by user (if we have multiple planes)
    public GameObject userPlane;
    private GameObject createdPlane;

    public Vector2 startPosNormal = new Vector2(0.5f, -0.1f);
    public Vector2 playPosNormal = new Vector2(0.5f, 0.3f);
    public Vector2 endPosNormal = new Vector2(0.5f, -1.5f);

    public UnityEvent OnPlayerReady;
    public UnityEvent OnPlayerLeave;

    public UnityEvent OnLevelFailed;

    public void Init()
    {
        //Initialize the plane, set it at the center
        CreateUserPlane(userPlane);
    }

    private void CreateUserPlane(GameObject prefab)
    {
        //If we already have a plane, then destroy it
        if (createdPlane != null)
            Destroy(createdPlane);

        //Create the new plane
        createdPlane = Instantiate<GameObject>(prefab);
        createdPlane.GetComponent<PlaneController>().enabled = false;
        createdPlane.GetComponent<FiringSystem>().enabled = false;
        createdPlane.GetComponent<PlaneControllerInput>().enabled = false;

        createdPlane.transform.position = Vector3.zero;
    }

    public void StartGame()
    {
        //Set the plane location and target
        Vector3 startPoint = Camera.main.ScreenToWorldPoint(new Vector3(startPosNormal.x * Screen.width, startPosNormal.y * Screen.height, 50));
        Vector3 endPoint = Camera.main.ScreenToWorldPoint(new Vector3(playPosNormal.x * Screen.width, playPosNormal.y * Screen.height, 50));
        //Set the start posittion
        createdPlane.transform.position = startPoint;
        createdPlane.GetComponent<SimpleMove>().SetPosition(endPoint);

        createdPlane.GetComponent<SimpleMove>().OnTargetReached.AddListener(OnTargetReachedStart);
        createdPlane.GetComponent<PlaneController>().OnPlaneHealthEmpty.AddListener(OnPlaneHealthEmpty);
        createdPlane.GetComponent<PlaneController>().OnPlaneHealthUpdated.RemoveListener(OnPlaneHealthUpdated);

        HealthBarUI healthBarUI = FindObjectOfType<HealthBarUI>();
        if(healthBarUI != null)
        {
            healthBarUI.Init(createdPlane.GetComponent<PlaneController>().GetHealthSystem());
        }
        else
        {
            Debug.Log("Could not find health bar");
        }



    }

    [ContextMenu("OnGameEnd")]
    public void OnGameEnd()
    {

        createdPlane.GetComponent<PlaneControllerInput>().enabled = false;
        createdPlane.GetComponent<FiringSystem>().enabled = false;

        //Set the plane location and target
        Vector3 endPoint = Camera.main.ScreenToWorldPoint(new Vector3(endPosNormal.x * Screen.width, endPosNormal.y * Screen.height, 50));

        //Set the start posittion
        createdPlane.GetComponent<SimpleMove>().SetPosition(endPoint);
        createdPlane.GetComponent<SimpleMove>().enabled = true;

        createdPlane.GetComponent<SimpleMove>().OnTargetReached.AddListener(OnTargetReachedEnd);
        createdPlane.GetComponent<PlaneController>().OnPlaneHealthEmpty.RemoveListener(OnPlaneHealthEmpty);
        createdPlane.GetComponent<PlaneController>().OnPlaneHealthUpdated.RemoveListener(OnPlaneHealthUpdated);

    }


    public void OnGameFailed()
    {
    }

    private void OnTargetReachedStart()
    {
        createdPlane.GetComponent<SimpleMove>().OnTargetReached.RemoveListener(OnTargetReachedStart);

        createdPlane.GetComponent<PlaneController>().enabled = true;
        createdPlane.GetComponent<FiringSystem>().enabled = true;
        createdPlane.GetComponent<PlaneControllerInput>().enabled = true;
        createdPlane.GetComponent<SimpleMove>().enabled = false;

        //On Game start actual
        OnPlayerReady.Invoke();
    }

    private void OnTargetReachedEnd()
    {
        createdPlane.GetComponent<SimpleMove>().OnTargetReached.RemoveListener(OnTargetReachedEnd);

        Destroy(createdPlane);

        //On Game end
        OnPlayerLeave.Invoke();
    }

    private void OnPlaneHealthUpdated()
    {
    }

    private void OnPlaneHealthEmpty()
    {
        Destroy(createdPlane);
        Debug.Log("Level failed");

        GameManger.instance.LevelFail();

    }
}
