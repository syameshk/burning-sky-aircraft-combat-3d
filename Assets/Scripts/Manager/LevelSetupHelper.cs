﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSetupHelper : MonoBehaviour
{

    public Vector2[] startPosNormal = { new Vector2(0.1f, 1.2f) };

    public LevelDetails[] levels;

    private List<GameObject> createdItems = new List<GameObject>();

    private LevelDetails selected;
    private float spawnDelay = 1;

    private int destroyCount = 0;
    private Coroutine coroutine;

    public void Reset()
    {
        //Reset everything

        if (coroutine != null)
            StopCoroutine(coroutine);
        coroutine = null;

        foreach (GameObject item in createdItems)
        {
            if (item != null)
                Destroy(item);
        }

        createdItems.Clear();
    }

    public void Init(int level = 1)
    {
        
        Debug.Log("Level Setup Init"+level);
        if (level < levels.Length)
            selected = levels[level];
        else
            selected = levels[levels.Length - 1];

        destroyCount = 0;
        createdItems = new List<GameObject>();

        coroutine =  StartCoroutine(EnemySpawn());
    }

    private IEnumerator EnemySpawn()
    {
        //For all waves
        for (int i = 0; i < selected.waves.Length; i++)
        {
            //for all enemies
            for (int j = 0; j < selected.waves[i].enemies.Length; j++)
            {
                //for each type enemy
                for (int k = 0; k < selected.waves[i].enemies[j].count; k++)
                {
                    CreateEnemyBasic(selected.waves[i].enemies[j].enemy.prefab, GetRandomSpawnPosition());
                    yield return new WaitForSeconds(spawnDelay);
                }
            }
        }
    }

    int posIndex = -1;
    private Vector3 GetRandomSpawnPosition()
    {
        posIndex++;
        if (posIndex >= startPosNormal.Length)
            posIndex = 0;
        return startPosNormal[posIndex];
    }

    private void CreateEnemyBasic(GameObject prefab, Vector2 startPosNormal)
    {
        //Set the plane location and target
        Vector3 startPoint = Camera.main.ScreenToWorldPoint(new Vector3(startPosNormal.x * Screen.width, startPosNormal.y * Screen.height, 50));
        Vector3 endPoint = Camera.main.ScreenToWorldPoint(new Vector3(startPosNormal.x * Screen.width, -0.2f * Screen.height, 50));

        //Create the new plane
        GameObject createdPlane = Instantiate<GameObject>(prefab);
        createdItems.Add(createdPlane);

        //Set the start posittion
        createdPlane.transform.position = startPoint;
        createdPlane.GetComponent<SimpleMove>().SetPosition(endPoint);

        createdPlane.GetComponent<PlaneController>().OnPlaneHealthEmpty.AddListener(OnPlaneHealthEmpty);

        Debug.Log("Created :"+createdItems.Count);
    }

    private void OnPlaneHealthEmpty()
    {
        Debug.Log("Destroyed :" + destroyCount);
        destroyCount++;
        if(destroyCount >= createdItems.Count)
        {
            Debug.Log("Level complted");
            GameManger.instance.LevelCompleted();
        }
    }
}
