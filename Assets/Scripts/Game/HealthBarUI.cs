﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarUI : MonoBehaviour
{
    public Image barGroup;
    private HealthSystem healthSystem;

    public void Init(HealthSystem healthSystem)
    {
        this.healthSystem = healthSystem;
        this.healthSystem.OnHealthUpdated.AddListener(OnHealthUpdated);
        this.barGroup.fillAmount = 1;
    }

    private void OnDestroy()
    {
        if (this.healthSystem != null)
            this.healthSystem.OnHealthUpdated.RemoveAllListeners();
    }

    void OnHealthUpdated()
    {
        barGroup.fillAmount = healthSystem.GetHealth();
    }
}
