﻿using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class HealthSystem
{
    public UnityEvent OnHealthUpdated;
    private int maxHealth = 100;
    [SerializeField]
    private int health;

    public HealthSystem(int maxHealth)
    {
        this.maxHealth = maxHealth;
        this.health = maxHealth;
        this.OnHealthUpdated = new UnityEvent();
    }

    public void Damage(int amount)
    {
        //Debug.Log("Damage"+amount);

        health -= amount;
        if (health < 0) health = 0;

        //Raise event???
        OnHealthUpdated.Invoke();
    }

    public void DamageFull()
    {
        health = 0;
        //Raise event???
        OnHealthUpdated.Invoke();
    }

    public void Heal(int amount)
    {
        health += amount;
        if (health > maxHealth) health = maxHealth;

        //Raise event???
        OnHealthUpdated.Invoke();
    }

    public float GetHealth()
    {
        return (float)health / maxHealth;
    }
}
