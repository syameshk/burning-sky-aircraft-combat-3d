﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfiniteScroll : MonoBehaviour
{
    public float scrollSpeed = 0.1f;
    private ScrollRect scrollRect;
    public float val = 0;

    private void Start()
    {
        scrollRect = GetComponent<ScrollRect>();
    }

    private void Update()
    {
        scrollRect.verticalNormalizedPosition = val;
        //scrollRect.verticalScrollbar.value = val;
        val += Time.deltaTime * scrollSpeed;
        if (val > 1)
            val = 0;
    }
}
