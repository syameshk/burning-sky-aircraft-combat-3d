﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlaneController : MonoBehaviour
{
    [Header("Plane Characteristics")]
    public float maxSpeed = 10;

    [SerializeField]
    private float sensitivity = 10f;

    public int maxHealth = 100;

    public HealthBar healthBar;

    private Rigidbody planeRB;

    private HealthSystem healthSystem;

    public UnityEvent OnPlaneHealthEmpty;
    public UnityEvent OnPlaneHealthUpdated;

    private void Awake()
    {
        planeRB = GetComponent<Rigidbody>();

        //Create the health system
        healthSystem = new HealthSystem(maxHealth);

        //Set up the health bar
        if (healthBar != null)
            healthBar.Init(healthSystem);
    }

    public void SetTargetPosition(Vector3 target)
    {
        //Find the velocity and make it move with physics
        //Calculate the direction
        Vector3 direction = target - transform.position;

        //Calculate the speed
        float requiredSpeed = direction.magnitude;
        if (requiredSpeed >= maxSpeed)
            requiredSpeed = maxSpeed;

        //Calculate the velocity
        Vector3 velocity = direction.normalized * requiredSpeed * sensitivity;
        //velocity = (point - planeRB.transform.position) * _sensitivity;

        //Add the velocity to player plane. Should try MovePosition(pos)? might be error free? can remove sensitivity?
        if(planeRB == null)
            planeRB = GetComponent<Rigidbody>();
        planeRB.velocity = velocity;
    }

    public void SetVelocity(Vector3 velocity)
    {
        //Add the velocity to player plane. Should try MovePosition(pos)? might be error free? can remove sensitivity?
        planeRB.velocity = velocity;
    }

    public HealthSystem GetHealthSystem()
    {
        return healthSystem;
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("OnTriggerEnter :" + gameObject.name + " v " + other.name);

        //If we hit by bullets
        if (other.gameObject.CompareTag("Bullets"))
        {
            //Delete the bullet
            Destroy(other.gameObject.transform.parent.gameObject);

            //Take damage
            BulletInfo bulletInfo = other.GetComponentInParent<BulletInfo>();
            healthSystem.Damage(bulletInfo.damage);

            //Hack for score now
            if (gameObject.CompareTag("Enemy"))
            {
                GameManger.instance.AddScore(bulletInfo.damage);
            }
        }

        //If enemy hit player
        if (other.gameObject.CompareTag("Player"))
        {
            //Full damage on enemy
            healthSystem.DamageFull();
        }
        
        //If player hit enemy
        if (other.gameObject.CompareTag("Enemy"))
        {
            //Player take some damage
            healthSystem.Damage(10);
        }

        //If player/enemy hit wall
        if (other.gameObject.CompareTag("Wall"))
        {
            //Full damage on enemy
            healthSystem.DamageFull();

            Debug.Log("Wall"+ healthSystem.GetHealth());
        }

        //Check if we lost all  health
        if (healthSystem.GetHealth() == 0)
        {
            OnPlaneHealthEmpty.Invoke();

            //Destroy if you are an enemy plane
            if (gameObject.CompareTag("Enemy"))
                Destroy(gameObject);
        }

        
    }

}
