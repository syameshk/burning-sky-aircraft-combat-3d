﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiringSystem : MonoBehaviour
{

    public GameObject bulletPrefab;
    public GameObject bulletPrefabMax;
    public Transform[] firePoints;


    public float firingRate = 1.0f;
    public float speed = 1.0f;
    public int damage = 5;


    private float lastTime = 0;

    // Start is called before the first frame update
    void Start()
    {
        lastTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time - lastTime > firingRate)
        {
            for (int i = 0; i < firePoints.Length; i++)
            {
                GameObject created = Instantiate<GameObject>(bulletPrefab);
                created.transform.position = firePoints[i].transform.position;

                //Set the values
                BulletInfo bulletInfo = created.GetComponent<BulletInfo>();
                bulletInfo.damage = damage;
                bulletInfo.speed = speed;

                //Make the bullet move
                Rigidbody rb = created.GetComponent<Rigidbody>();
                rb.velocity = firePoints[i].forward * speed;
            }

            lastTime = Time.time;
        }
    }
}
