﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(PlaneController))]
public class SimpleMove : MonoBehaviour
{
    public Vector3 target = new Vector3(0,0,-35);

    public UnityEvent OnTargetReached;

    protected PlaneController planeController;

    public virtual void SetPosition(Vector3 target)
    {
        Debug.Log("Target :"+target.ToString("F3"));
        this.target = target;
        planeController = GetComponent<PlaneController>();
        planeController.SetTargetPosition(target);
    }

    public void FixedUpdate()
    {
        UpdateMove();
    }

    public virtual void UpdateMove()
    {
        if (Vector3.Distance(transform.position, target) < 0.25f)
        {
            OnTargetReached.Invoke();

        }
    }

}
