﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletInfo : MonoBehaviour
{
    public int damage;
    public float speed;

    private void Awake()
    {
        Destroy(gameObject, 20);
    }

    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(gameObject.name);
        Destroy(gameObject);
    }

}
