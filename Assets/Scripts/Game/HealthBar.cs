﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    public GameObject barGroup;
    private HealthSystem healthSystem;

    public void Init(HealthSystem healthSystem)
    {
        this.healthSystem = healthSystem;
        this.healthSystem.OnHealthUpdated.AddListener(OnHealthUpdated);
    }

    private void OnDestroy()
    {
        if(this.healthSystem != null)
            this.healthSystem.OnHealthUpdated.RemoveAllListeners();
    }

    void OnHealthUpdated()
    {
        barGroup.transform.localScale = new Vector3(healthSystem.GetHealth(), 1, 1);
    }
}
