﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePathMove : SimpleMove
{
    public Vector2[] pathNormalPoints = { new Vector2(0.2f, 0.8f), new Vector2(0.5f, 0.7f), new Vector2(0.8f, 0.8f), new Vector2(0.5f, 0.8f) };
    public int index = 0;

    public override void SetPosition(Vector3 target)
    {
        index = 0;
        Vector3 resPoint = new Vector3(pathNormalPoints[0].x * Screen.width, pathNormalPoints[0].y * Screen.height, 50);
        this.target = Camera.main.ScreenToWorldPoint(new Vector3(pathNormalPoints[0].x * Screen.width, pathNormalPoints[0].y * Screen.height, 50));
        Debug.Log("SimplePathMove Target :" + this.target.ToString("F3"));

        planeController = GetComponent<PlaneController>();
        planeController.SetTargetPosition(this.target);
    }

    public override void UpdateMove()
    {
        if (Vector3.Distance(transform.position, target) < 0.25f)
        {
            index++;
            if (index >= pathNormalPoints.Length)
                index = 0;

            //Set the new target
            target = Camera.main.ScreenToWorldPoint(new Vector3(pathNormalPoints[index].x * Screen.width, pathNormalPoints[index].y * Screen.height, 50));
            planeController.SetTargetPosition(this.target);

            Debug.Log(target.ToString("F2"));

            OnTargetReached.Invoke();

        }
    }
}
