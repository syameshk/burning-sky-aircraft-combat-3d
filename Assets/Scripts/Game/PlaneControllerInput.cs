﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlaneController))]
public class PlaneControllerInput : MonoBehaviour
{
    [Header("The range for the cursor move")]
    public Vector2 minPos = new Vector2(0.1f, 0.1f);
    public Vector2 maxPos = new Vector2(0.9f, 0.9f);
    [Header("The height in which camera is placed")]
    public float cameraHeight = 50;
    [Header("The layer to be tested for touch placement")]
    public LayerMask layerMask;
    [Header("Main camera cache")]
    public Camera cameraCache;


    private Vector3 minPosWorld = new Vector2(0.1f, 0.1f), maxPosWorld = new Vector2(0.9f, 0.9f);
    private bool isOnPlane = false;
    private PlaneController planeController;

    private void Start()
    {
        planeController = GetComponent<PlaneController>();
        cameraCache = Camera.main;
        minPosWorld = cameraCache.ScreenToWorldPoint(new Vector3(minPos.x * Screen.width, minPos.y * Screen.height, cameraHeight));
        maxPosWorld = cameraCache.ScreenToWorldPoint(new Vector3(maxPos.x * Screen.width, maxPos.y * Screen.height, cameraHeight));
    }

    //This is for test, input should be taken from another class
    private void Update()
    {
        //Check for click/touch
        Vector2 touchPosition = Vector2.zero;
        Vector3 target = Vector3.zero;
        if (Try(out touchPosition))
        {
            if (isOnPlane || OnInteractonTest(touchPosition))
            {
                //Set the position
                target = OnInteractonMove(touchPosition);
                isOnPlane = true;
            }
            else
            {
                //Check if we touched on the plane, if yes, then move
                isOnPlane = false;
            }
        }
        else
        {
            isOnPlane = false;
        }

        //If we are not controlling plane, then make the velocity zero
        if (!isOnPlane)
            planeController.SetVelocity(target);
        else
            planeController.SetTargetPosition(target);
    }

    public bool OnInteractonTest(Vector2 touchPosition)
    {
        // Construct a ray from the current touch coordinates
        Ray ray = cameraCache.ScreenPointToRay(touchPosition);
        return Physics.Raycast(ray, 500, layerMask);
    }

    public Vector3 OnInteractonMove(Vector2 touchPosition)
    {
        //The world point of touch
        Vector3 point = cameraCache.ScreenToWorldPoint(new Vector3(touchPosition.x, touchPosition.y, cameraHeight));
        //Debug.Log(string.Format("{0}:{1}", touchPosition, point));

        //Clamp the position value
        point = new Vector3(Mathf.Clamp(point.x, minPosWorld.x, maxPosWorld.x), point.y, Mathf.Clamp(point.z, minPosWorld.z, maxPosWorld.z));

        return point;
    }

    private bool _isTouching = false;
    public bool Try(out Vector2 touchPosition)
    {
        if (Application.isEditor)
        {
            if (Input.GetMouseButton(0))
            {
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    touchPosition = Vector2.zero;
                    return false;
                }

                touchPosition = Input.mousePosition;
                return true;
            }
            touchPosition = Vector2.zero;
            return false;
        }

        if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
        {

            if (Input.touches.Select(touch => touch.fingerId).Any(id => EventSystem.current.IsPointerOverGameObject(id)))
            {
                touchPosition = Vector2.zero;
                return false;
            }
            _isTouching = true;
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
        else if (Input.touchCount == 1 && _isTouching)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
        _isTouching = false;

        touchPosition = Vector2.zero;
        return false;
    }
}
