﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level01", menuName = "Data/Level", order = 1)]
public class LevelDetails : ScriptableObject
{
    [System.Serializable]
    public struct EnemyCount
    {
        public EnemyDetails enemy;
        public int count;
    }

    [System.Serializable]
    public struct WaveData
    {
        public EnemyCount[] enemies;
    }

    public WaveData[] waves;
}
