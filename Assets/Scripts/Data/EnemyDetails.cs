﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy01", menuName = "Data/Enemy", order = 2)]
public class EnemyDetails : ScriptableObject
{
    public int enemyLevel;
    public GameObject prefab;
}
